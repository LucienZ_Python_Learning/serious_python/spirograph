import argparse
import turtle
from datetime import datetime
from .spiro import Spiro
from .spiro_animator import SpiroAnimator
from PIL import Image


def save_drawing():
    # hide the turtle cursor
    turtle.hideturtle()

    # generate unique filename
    date_str = (datetime.now()).strftime("%d%b%Y.%H%M%S")
    filename = 'spiro-' + date_str
    print ('saving drawing to %s.eps/png' % filename)

    # get the tkinter canvas
    canvas = turtle.getcanvas()

    # save the drawing as a postscript image
    canvas.postscript(file = filename + '.eps')

    # use the pillo module to convert the postscript image file to PNG
    img = Image.open(filename + '.eps')
    img.save(filename + '.png', 'png')

    # show the turtle cursor
    turtle.showturtle()

def test():
    print("€dsafdas")

# main() function
def launch():
    # use sys.argv if needed
    print('generating spirograph...')

    """
    Terminology:
    R: radius of outer circle
    r: radius of inner circle
    l: ratio of hole distance to r
    """
    desc_str = 'drawing of spirographs'

    parser = argparse.ArgumentParser(description=desc_str)

    # add expected arguments

    parser.add_argument('--sparams', nargs=3, dest='sparams', required=False,
                        help="The three arguments in sparams: R, r, l.")

    args = parser.parse_args()

    # set the width of the drawing window to 80 % of the screen width
    turtle.setup(width=0.8)

    # set the cursor shape to turtle
    turtle.shape('turtle')

    # set the title to Spirographs !
    turtle.title('Spirographs')

    # # add the key handler to save our drawings
    turtle.onkey(save_drawing, 's')

    # start listening
    turtle.listen()

    # hide the main turtle cursor
    turtle.hideturtle()

    # check for any arguments sent to --sparams and draw the
    if args.sparams:
        params = [float(x) for x in args.sparams]
        params[0] = int(params[0])
        params[1] = int(params[1])

        # draw the Spirograph with the given parameters
        col = (0.0, 0.0, 0.0)
        spiro = Spiro(0, 0, col, *params)
        spiro.draw()
    else:
        # create the animator object
        spiro_anim = SpiroAnimator(4)

        # add a key handler to toggle the turtle cursor
        turtle.onkey(test, "t")

        # add a key handler to restart the animation
        turtle.onkey(test, "space")

    # start the turtle main loop
    turtle.mainloop()
