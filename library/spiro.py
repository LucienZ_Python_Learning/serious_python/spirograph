import math
import turtle


# a class that draws a Spirograph
class Spiro:
    # constructor
    def __init__(self, xc, yc, col, R, r, l):
        # create the turtle object
        self.t = turtle.Turtle()

        # set the cursor shape
        self.t.shape('turtle')

        self.t.speed(10)

        # set the step in degrees
        self.step = 5

        # set the drawing complete flag
        self.drawingComplete = False

        # set the parameters
        self.set_params(R, col, l, r, xc, yc)

        # initialize the drawing
        self.restart()

    # set the parameters
    def set_params(self, R, col, l, r, xc, yc):
        # the Spirograph parameters
        self.xc = xc
        self.yx = yc
        self.col = col
        self.R = R
        self.r = r
        self.l = l
        # reduce r/R to its smallest form by dividing with the
        gcd_val = math.gcd(self.r, self.R)
        self.n_rot = self.r // gcd_val
        # get ratio of radii
        self.k = r / float(R)
        # set the color
        self.t.color(*col)
        # store the current angle
        self.a = 0

    # restart the drawing
    def restart(self):
        # set the flag
        self.drawingComplete = False

        # show the turtle
        self.t.showturtle()

        # go to the first point
        self.t.up()
        R, k, l = self.R, self.k, self.l
        a = 0.0
        x = R * ((1 - k) * math.cos(a) + l * k * math.cos((1 - k) * a / k))
        y = R * ((1 - k) * math.sin(a) + l * k * math.sin((1 - k) * a / k))
        self.t.setpos(self.xc + x, self.yx + y)
        self.t.down()

    # draw the whole thing
    def draw(self):
        # draw the rest of the points
        R, k, l = self.R, self.k, self.l
        for i in range(0, 360 * self.n_rot + 1, self.step):
            a = math.radians(i)
            x = R * ((1 - k) * math.cos(a) + l * k * math.cos((1 - k) * a / k))
            y = R * ((1 - k) * math.sin(a) + l * k * math.sin((1 - k) * a / k))
            self.t.setpos(self.xc + x, self.yx + y)
        # drawing is now done so hide the turtle cursor
        self.drawingComplete = True
        self.t.hideturtle()

    # update by one step
    def update(self):
        # skip the rest of the steps if done
        if self.drawingComplete:
            return

        # increment the angle
        self.a += self.step

        # draw a step
        R, k, l = self.R, self.k, self.l

        # set the angle
        a = math.radians(self.a)
        x = R * ((1 - k) * math.cos(a) + l * k * math.cos((1 - k) * a / k))
        y = R * ((1 - k) * math.sin(a) + l * k * math.sin((1 - k) * a / k))
        self.t.setpos(self.xc + x, self.yx + y)

        # if drawing is complete, set the flag
        if self.a >= 360 * self.n_rot:
            self.drawingComplete = True

            # drawing is now done so hide the turtle cursor
            self.t.hideturtle()

    # clear everything
    def clear(self):
        self.t.clear()
