import turtle
import random
from .spiro import Spiro


# a class for animating Spirographs
class SpiroAnimator:
    # constructor
    def __init__(self, number_of_spirographs):
        # set the timer value in milliseconds
        self.delta_t = 15

        # get the windows dimensions
        self.width = turtle.window_width()
        self.height = turtle.window_height()

        # create the Spiro objects
        self.spiros = []
        for i in range(number_of_spirographs):

            # generate random parameters
            r_params = self.generate_random_params()

            # set the spiro parameters
            spiro = Spiro(*r_params)
            self.spiros.append(spiro)
            # call timer
            turtle.ontimer(self.update, self.delta_t)

    # this method generate the params of a spiro randomly
    def generate_random_params(self):
        width, height = self.width, self.height
        R = random.randint(50, min(width, height) // 2)
        r = random.randint(10, 9 * R // 10)
        l = random.uniform(0.1, 0.9)
        xc = random.randint(-width // 2, width // 2)
        yc = random.randint(-height // 2, height // 2)
        col = (random.random(), random.random(), random.random())
        return xc, yc, col, R, r, l

    # restart spiro drawing
    def restart(self):
        print("prout")
        for spiro in self.spiros:
            # clear
            spiro.clear()

            # generate random parameters
            r_params = self.generate_random_params()

            # set the spiro parameters
            spiro.set_params(*r_params)

            # restart drawing
            spiro.restart()

    def update(self):
        # update all spiros
        n_complete = 0
        for spiro in self.spiros:
            # update
            spiro.update()

            # count completed spiros
            if spiro.drawingComplete:
                n_complete += 1

        # restart if all spiros are complete
        if n_complete == len(self.spiros):
            self.restart()

        # call the timer
        turtle.ontimer(self.update(), self.delta_t)

    # toggle turtle cursor on and off
    def toggle_turtles(self):
        print("prout")
        for spiro in self.spiros:
            if spiro.t.isvisible():
                spiro.t.hideturtle()
            else:
                spiro.t.showturtle()

